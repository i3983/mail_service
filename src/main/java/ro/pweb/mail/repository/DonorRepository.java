package ro.pweb.mail.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.pweb.mail.model.Donor;

import java.util.List;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

@Repository
public interface DonorRepository extends JpaRepository<Donor, Integer> {

    @Query("SELECT u FROM Donor u WHERE u.donorEmail = ?1")
    public Donor findDonorByEmail(String email);

    @Query("SELECT u.donorEmail FROM Donor u WHERE u.subscribedRequirements = true")
    public List<String> getSubscribedDonors();

    @Modifying
    @Query("update Donor u set u.subscribedRequirements = true where u.id = ?1")
    public int subscribeNewsletter(int id);

    @Modifying
    @Query("update Donor u set u.subscribedRequirements = false where u.id = ?1")
    public int unsubscribeNewsletter(int id);
}

