package ro.pweb.mail.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ro.pweb.mail.model.Recipient;


import java.util.List;

public interface RecipientRepository extends JpaRepository<Recipient, Integer> {

    @Query("SELECT u FROM Recipient u WHERE u.recipientEmail = ?1")
    public Recipient findRecipientByEmail(String email);

    @Query("SELECT u.recipientEmail FROM Recipient u WHERE u.subscribedDonations = true")
    public List<String> getSubscribedRecipients();

    @Modifying
    @Query("update Recipient u set u.subscribedDonations = true where u.id = ?1")
    public int subscribeNewsletter(int id);

    @Modifying
    @Query("update Recipient u set u.subscribedDonations = false where u.id = ?1")
    public int unsubscribeNewsletter(int id);
}

