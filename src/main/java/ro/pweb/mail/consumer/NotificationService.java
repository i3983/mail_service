package ro.pweb.mail.consumer;

import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import ro.pweb.mail.repository.DonorRepository;
import ro.pweb.mail.repository.RecipientRepository;
import ro.pweb.mail.response.DonationResponse;
import ro.pweb.mail.response.ItemDonationResponse;
import ro.pweb.mail.response.ItemRequirementResponse;
import ro.pweb.mail.response.RequirementResponse;


import java.util.List;

@Component
public class NotificationService {

    @Autowired
    private RecipientRepository recipientRepository;

    @Autowired
    private DonorRepository donorRepository;

    private JavaMailSender javaMailSender;
    Logger logger = LoggerFactory.getLogger(NotificationService.class);

    @Autowired
    public NotificationService(JavaMailSender javaMailSender){
        this.javaMailSender = javaMailSender;
    }

    public void sendDonationNotification(DonationResponse donation){
        logger.info("donation notification");
        List<String> recipients = recipientRepository.getSubscribedRecipients();
        logger.info("Recipients: " + recipients.toString());
        StringBuilder mailText = new StringBuilder("A new donation post was added on the platform Help Without Borders\n" +
                "Details of the donation:\n" +
                "Title: " + donation.getPostTitle() +
                "\nItems donated:\n");
        for (ItemDonationResponse item: donation.getItems()){
            mailText.append(item.getItemName()).append("   ").append(item.getQuantity()).append("\n");
        }
        mailText.append("\n\nThank you!\nHelp Without Borders team\n");

        for (String recipient : recipients){
            logger.info(recipient);
            SimpleMailMessage mail = new SimpleMailMessage();
            mail.setTo(recipient);
            mail.setFrom("help.without.borders.pweb@gmail.com");
            mail.setSubject("New donation added");
            mail.setText(mailText.toString());
            javaMailSender.send(mail);
        }
    }

    public void sendRequirementNotification(RequirementResponse requirement){
        logger.info("requirement notification");
        List<String> donors = donorRepository.getSubscribedDonors();
        logger.info("Donors: " + donors.toString());
        StringBuilder mailText = new StringBuilder("A new requirement post was added on the platform Help Without Borders\n" +
                "Details of the requirement:\n" +
                "Title: " + requirement.getPostTitle() +
                "\nItems required:\n");
        for (ItemRequirementResponse item: requirement.getItems()){
            mailText.append(item.getItemName()).append("   ").append(item.getQuantity()).append("\n");
        }
        mailText.append("\n\nThank you!\nHelp Without Borders team\n");

        for (String donor : donors){
            logger.info(donor);
            SimpleMailMessage mail = new SimpleMailMessage();
            mail.setTo(donor);
            mail.setFrom("help.without.borders.pweb@gmail.com");
            mail.setSubject("New requirement added");
            mail.setText(mailText.toString());
            javaMailSender.send(mail);
        }
    }

    public void sendNotification(Notification notification) throws MailException {
        if ("donation".equals(notification.getType())){
            logger.info("donation notification 2");
            sendDonationNotification(notification.getDonation());
        } else if ("requirement".equals(notification.getType())) {
            sendRequirementNotification(notification.getRequirement());
        }
    }

    @RabbitListener(queues = "pweb-queue")
    public void consumeMessageFromQueue(Notification notification){
        logger.info("Consumed message from queue from another service" + notification);
        sendNotification(notification);
        logger.info("message sent from another service");
    }
}

