package ro.pweb.mail.consumer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ro.pweb.mail.response.DonationResponse;
import ro.pweb.mail.response.RequirementResponse;


@Data
@NoArgsConstructor
@ToString
public class Notification {
    String type;
    DonationResponse donation;
    RequirementResponse requirement;

    public Notification(String type, DonationResponse donation) {
        this.type = type;
        this.donation = donation;
    }

    public Notification(String type, RequirementResponse requirement) {
        this.type = type;
        this.requirement = requirement;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public DonationResponse getDonation() {
        return donation;
    }

    public void setDonation(DonationResponse donation) {
        this.donation = donation;
    }

    public RequirementResponse getRequirement() {
        return requirement;
    }

    public void setRequirement(RequirementResponse requirement) {
        this.requirement = requirement;
    }
}
