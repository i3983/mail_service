package ro.pweb.mail.model;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Donor")
public class Donor{
    @Id
    @Column(name="donorId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name="identityGuid")
    private String identityGuid;

    @Column(name="donorEmail")
    private String donorEmail;

    @Column(name="phoneNumber")
    private String phoneNumber;

    @Column(name="address")
    private String address;

    @Column(name="name")
    private String name;

    @Column(name="subscribedRequirements")
    public boolean subscribedRequirements;

    @OneToMany(mappedBy="donor")
    private List<Requirement> requirements;

    @OneToMany(mappedBy="donor", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Donation> donations;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdentityGuid() {
        return identityGuid;
    }

    public void setIdentityGuid(String identityGuid) {
        this.identityGuid = identityGuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDonorEmail() {
        return donorEmail;
    }

    public void setDonorEmail(String donorEmail) {
        this.donorEmail = donorEmail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isSubscribedRequirements() {
        return subscribedRequirements;
    }

    public void setSubscribedRequirements(boolean subscribedRequirements) {
        this.subscribedRequirements = subscribedRequirements;
    }

    public List<Requirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(List<Requirement> requirements) {
        this.requirements = requirements;
    }

    public List<Donation> getDonations() {
        return donations;
    }

    public void setDonations(List<Donation> donations) {
        this.donations = donations;
    }
}
