package ro.pweb.mail.model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;

@Entity
@Table(name = "ItemDonation")
public class ItemDonation {
    @Id
    @Column(name="itemId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int item_id;

    @Column(name="itemName")
    private String itemName;

    @Column(name="quantity")
    private Double quantity;

    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "donationId")
    private Donation donation;

    public ItemDonation() {
    }

    public ItemDonation(int item_id, String item_name, Double quantity) {
        this.item_id = item_id;
        this.itemName = item_name;
        this.quantity = quantity;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Donation getDonation() {
        return donation;
    }

    public void setDonation(Donation donation) {
        this.donation = donation;
    }

    @Override
    public String toString() {
        return "ItemDonation{" +
                "item_id=" + item_id +
                ", item_name='" + itemName + '\'' +
                ", quantity=" + quantity +
                ", donation=" + donation +
                '}';
    }
}
