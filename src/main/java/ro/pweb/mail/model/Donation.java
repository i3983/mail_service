package ro.pweb.mail.model;



import org.hibernate.annotations.Cascade;
import ro.pweb.mail.response.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="Donation")
public class Donation {
    @Id
    @Column(name="donationId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int donationId;

    @Column(name="postTitle")
    private String postTitle;

    @Column(name="publishDate")
    private Date publishDate;

    @Column(name="status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToMany(mappedBy = "donation", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ItemDonation> items;

    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name="donorId", nullable = false)
    private Donor donor;

    @ManyToOne
    @JoinColumn(name="recipientId")
    private Recipient recipient;

    public int getDonationId() {
        return donationId;
    }

    public void setDonationId(int donationId) {
        this.donationId = donationId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public List<ItemDonation> getItems() {
        return items;
    }

    public void setItems(List<ItemDonation> items) {
        this.items = items;
    }

    public Donor getDonor() {
        return donor;
    }

    public void setDonor(Donor donor) {
        this.donor = donor;
    }

    public Recipient getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Donation{" +
                "donationId=" + donationId +
                ", postTitle='" + postTitle + '\'' +
                ", publishDate=" + publishDate +
                ", status=" + status +
                ", itemDonations=" + items +
                ", donor=" + donor +
                ", recipient=" + recipient +
                '}';
    }
}

