package ro.pweb.mail.model;

import org.hibernate.annotations.Cascade;
import ro.pweb.mail.response.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="Requirement")
public class Requirement {
    @Id
    @Column(name="requirementId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int requirementId;

    @Column(name="postTitle")
    private String postTitle;

    @Column(name="publishDate")
    private Date publishDate;

    @Column(name="status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToMany(mappedBy = "requirement",  cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ItemRequirement> items;

    @ManyToOne
    @JoinColumn(name="donorId")
    private Donor donor;

    @ManyToOne(fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name="recipientId", nullable = false)
    private Recipient recipient;

    public Requirement() {
    }

    public int getRequirementId() {
        return requirementId;
    }

    public void setRequirementId(int requirementId) {
        this.requirementId = requirementId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public List<ItemRequirement> getItems() {
        return items;
    }

    public void setItems(List<ItemRequirement> items) {
        this.items = items;
    }

    public Donor getDonor() {
        return donor;
    }

    public void setDonor(Donor donor) {
        this.donor = donor;
    }

    public Recipient getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Requirement{" +
                "requirementId='" + requirementId + '\'' +
                ", postTitle='" + postTitle + '\'' +
                ", publishDate=" + publishDate +
                ", status=" + status +
                ", items=" + items +
                ", donor=" + donor +
                ", recipient=" + recipient +
                '}';
    }
}

