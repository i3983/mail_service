package ro.pweb.mail.model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;

@Entity
@Table(name = "ItemRequirement")
public class ItemRequirement {

    @Id
    @Column(name="itemId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int item_id;

    @Column(name="itemName")
    private String itemName;

    @Column(name="quantity")
    private Double quantity;

    @ManyToOne
    @Cascade(CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "requirementId")
    private Requirement requirement;

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String item_name) {
        this.itemName = item_name;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Requirement getRequirement() {
        return requirement;
    }

    public void setRequirement(Requirement requirement) {
        this.requirement = requirement;
    }

    @Override
    public String toString() {
        return "ItemRequirement{" +
                "item_id=" + item_id +
                ", item_name='" + itemName + '\'' +
                ", quantity=" + quantity +
                ", requirement=" + requirement +
                '}';
    }
}

