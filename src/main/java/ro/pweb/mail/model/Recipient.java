package ro.pweb.mail.model;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name ="Recipient")
public class Recipient{
    @Id
    @Column(name="recipientId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name="identityGuid")
    private String identityGuid;

    @Column(name="recipientEmail")
    private String recipientEmail;

    @Column(name="phoneNumber")
    private String phoneNumber;

    @Column(name="address")
    private String address;

    @Column(name="name")
    private String name;

    @Column(name="subscribedDonations")
    public boolean subscribedDonations;

    @OneToMany(mappedBy="recipient", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Requirement> requirements;

    @OneToMany(mappedBy="recipient")
    private List<Donation> donations;

    public Recipient() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdentityGuid() {
        return identityGuid;
    }

    public void setIdentityGuid(String identityGuid) {
        this.identityGuid = identityGuid;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSubscribedDonations() {
        return subscribedDonations;
    }

    public void setSubscribedDonations(boolean subscribedDonations) {
        this.subscribedDonations = subscribedDonations;
    }

    public List<Requirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(List<Requirement> requirements) {
        this.requirements = requirements;
    }

    public List<Donation> getDonations() {
        return donations;
    }

    public void setDonations(List<Donation> donations) {
        this.donations = donations;
    }

    @Override
    public String toString() {
        return "Recipient{" +
                "id=" + id +
                ", identityGuid='" + identityGuid + '\'' +
                ", recipientEmail='" + recipientEmail + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                ", subscribedDonations=" + subscribedDonations +
                ", requirements=" + requirements +
                ", donations=" + donations +
                '}';
    }
}
