package ro.pweb.mail.response;

public enum Status {
    ACTIVE,
    PENDING,
    CLOSED_BY_RECIPIENT,
    CLOSED
}
